# NoFFMPEG

## Description
NoFFMPEG is a plugin for [Gallery 3](http://gallery.menalto.com/) which will override Gallery 3's default helpers/movie.php file with one that has been modified to work without FFMPEG present on the web server, allowing users to upload video's in supported formats (FLV, MP4, and M4V).  Uploaded videos will be given a default thumbnail, and the video width and height will be set to 320x240.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.  There is nothing else to configure with this module.

## History
**Version 1.3.0:**
> - Compatibility fixes for Gallery 3.0.3.
> - Released 16 April 2012.
>
> Download: [Version 1.3.0](/uploads/02151bc87de15367a3b860d76c96a363/noffmpeg130.zip)

**Version 1.2.0:**
> - Compatibility fixes for Gallery 3.0.1.
> - Fixed issues with the upload from hiding videos and displaying a videos not accepted message.
> - Released 21 April 2011.
>
> Download: [Version 1.2.0](/uploads/8788b2e4fcf56fa3c63f7e02d8802e2a/noffmpeg120.zip)

**Version 1.1.0:**
> - Added the ability to auto-detect the video resolution of .flv videos.
> - Released 17 September 2010.
>
> Download: [Version 1.1.0](/uploads/cc3f3c26c286851e0d9b3e670c93716f/noffmpeg110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 14 September 2010.
>
> Download: [Version 1.0.0](/uploads/3425cc5f3136fc9dd897d3483316f1fe/noffmpeg100.zip)
